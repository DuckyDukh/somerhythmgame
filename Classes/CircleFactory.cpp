#include "CircleFactory.h"
#include "Game.h"
#include "Helper.h"
using namespace cocos2d;

CircleFactory::CircleFactory(Json::Value& aTabs)
{
	tabs = aTabs;
}

CircleFactory::~CircleFactory()
{
}

void CircleFactory::startProducing()
{
	for (int i = 0; i < 3; i++)
		weights[i] = 10;

	if (tabs)
	{
		for (auto& note : tabs) {
			float time = note["time"].asFloat();

			auto circle = std::make_shared<Circle>(getRandLine());
			circle->setTimeOnScreen(timeOnScreen);
			circle->setHitTime(time);
			circle->createAction();
			circleList.push_back(circle);
		}
		circleList.sort([this](std::shared_ptr<Circle> left, std::shared_ptr<Circle> right) 
		{ 
			return left->getHitTime() < right->getHitTime(); 
		});
	}
}

void CircleFactory::setDifficulty(int aDiff)
{
	switch (aDiff)
	{
	case DIFFICULTY::EASY:
		timeOnScreen = CONSTANTS->json["timeOnScreen"]["easy"].asFloat();
		break;
	case DIFFICULTY::NORMAL:
		timeOnScreen = CONSTANTS->json["timeOnScreen"]["normal"].asFloat();
		break;
	case DIFFICULTY::HARD:
		timeOnScreen = CONSTANTS->json["timeOnScreen"]["hard"].asFloat();
		break;
	default:
		break;
	}
}

int CircleFactory::getRandLine() 
{
	int sum = 0;
	for (int i = 0; i < 3; i++)
		sum += weights[i];
	int rand = random() % sum;

	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			if (weights[j] <= 0)
				weights[j] = 10;
		}

		if (weights[i] > rand)
		{
			weights[i] -= 1;
			return i;
		}
		rand -= weights[i];
	}
	return 0;
}

