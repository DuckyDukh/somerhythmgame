#pragma once
#include <cocos2d.h>
#include <Json/json.h>
#include "Circle.h"

namespace cocos2d
{
	class CircleFactory
	{
	public:
		CircleFactory(Json::Value& aTabs);
		~CircleFactory();
		void startProducing();
		void setDifficulty(int aDiff);

		std::list<std::shared_ptr<Circle>> circleList;
	private:
		int getRandLine();
		int weights[3];

		Json::Value tabs;
		float timeOnScreen;
	};
}


