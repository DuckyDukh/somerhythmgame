#include "SettingsWindow.h"
#include "Helper.h"
#include "ui/CocosGUI.h"
using namespace cocos2d;

SettingsWindow::SettingsWindow()
{
    Vec2 origin = PANEL_SETTINGS->origin;
    Size visibleSize = PANEL_SETTINGS->visibleSize;

    auto bg = Sprite::create("Images/Windows/SettingsBG.png");
    bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.8));
    this->addChild(bg);

    auto backButton = ui::Button::create("Images/UI/back.png");
    backButton->setPosition(Vec2(bg->getPositionX(), bg->getPositionY() - 85));
    backButton->addTouchEventListener([=](Ref* sender, ui::Widget::TouchEventType type) 
    {
        switch (type)
        {
        case ui::Widget::TouchEventType::ENDED:
            this->removeFromParent();
            break;
        default:
            break;
        }
    });
    this->addChild(backButton);

    auto slider = ui::Slider::create();
    slider->loadBarTexture("Images/UI/sliderBack.png");
    slider->loadProgressBarTexture("Images/UI/sliderFront.png");
    slider->setPosition(Vec2(bg->getPositionX(), bg->getPositionY() - 45));

    slider->setPercent(SETTINGS->getVolume() * 100);
    slider->addTouchEventListener([=](Ref* sender, ui::Widget::TouchEventType type) 
    {
        switch (type)
        {
        case ui::Widget::TouchEventType::ENDED:
        {
            SETTINGS->setVolume(slider->getPercent() / 100.f);
            break;
        }
        default:
            break;
        }
    });
    this->addChild(slider);

    auto text = Label::createWithTTF("0", "fonts/arial.ttf", 32);
    text->setTextColor(Color4B::WHITE);
    text->enableOutline(Color4B::BLACK, 3);
    text->setPosition(bg->getPositionX(), bg->getPositionY());
    text->setString("VOLUME");
    this->addChild(text);

    for (auto& child : this->getChildren())
    {
        child->setGlobalZOrder(15);
    }
}

SettingsWindow::~SettingsWindow()
{
}
