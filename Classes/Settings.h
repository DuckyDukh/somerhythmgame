#pragma once
#include "cocos2d.h"
#include <json/json.h>

namespace cocos2d
{
	class Settings
	{
	public:
		Settings();
		~Settings();
		float getVolume();
		void setVolume(float value);
	private:
		void save();

		Json::Value root;
	};
}