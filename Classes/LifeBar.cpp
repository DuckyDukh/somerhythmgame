#include "LifeBar.h"
#include "Helper.h"

using namespace cocos2d;

LifeBar::LifeBar()
{
	healingPower = 0;
	damagePower = 0;
	life = 100;
	drainRate = CONSTANTS->json["life"]["drainRate"].asFloat();

	Vec2 origin = PANEL_SETTINGS->origin;
	Size visibleSize = PANEL_SETTINGS->visibleSize;

	auto barView = Sprite::create("Images/Game/lifeBar.png");
	hpBar = ProgressTimer::create(barView);

	hpBar->setType(ProgressTimer::Type::BAR);
	hpBar->setBarChangeRate(Vec2(1, 0));
	hpBar->setAnchorPoint(Vec2(0, 1));
	hpBar->setPosition(0, origin.y + visibleSize.height);
	hpBar->setVisible(true);
	hpBar->setPercentage(life);

	this->addChild(hpBar);
}

LifeBar::~LifeBar()
{
}

void LifeBar::startDraining()
{
	this->scheduleUpdate();
}

void LifeBar::update(float delta)
{
	life -= delta * drainRate;
	hpBar->setPercentage(life);
}

void LifeBar::setDifficulty(int aDiff)
{
	switch (aDiff)
	{
	case DIFFICULTY::EASY:
	{
		healingPower = CONSTANTS->json["healingPower"]["easy"].asFloat();
		damagePower = CONSTANTS->json["damagePower"]["easy"].asFloat();
		break;
	}
	case DIFFICULTY::NORMAL:
	{
		healingPower = CONSTANTS->json["healingPower"]["normal"].asFloat();
		damagePower = CONSTANTS->json["damagePower"]["normal"].asFloat();
		break;
	}
	case DIFFICULTY::HARD:
	{
		healingPower = CONSTANTS->json["healingPower"]["hard"].asFloat();
		damagePower = CONSTANTS->json["damagePower"]["hard"].asFloat();
		break;
	}
	default:
		break;
	}
}

void LifeBar::damage()
{
	life -= damagePower;
}

void LifeBar::heal()
{
	life += healingPower;
}

void LifeBar::reset()
{
	life = 100;
	hpBar->setPercentage(life);
	this->unscheduleUpdate();
}