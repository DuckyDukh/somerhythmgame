#include "UI.h"
#include "Helper.h"
#include "AudioEngine.h"
#include "Game.h"
#include "SettingsWindow.h"
#include "MainMenu.h"
using namespace cocos2d;

UI::UI(Game* aParent)
{
	gameScene = aParent;

	origin = PANEL_SETTINGS->origin;
	visibleSize = PANEL_SETTINGS->visibleSize;

	int dx = PANEL_SETTINGS->root["GameScreen"]["PauseButton"]["dx"].asInt();
	int dy = PANEL_SETTINGS->root["GameScreen"]["PauseButton"]["dy"].asInt();

	auto pauseButton = ui::Button::create("Images/UI/pause.png");
	pauseButton->setPosition(Vec2(origin.x + visibleSize.width + dx, origin.y + visibleSize.height + dy));
	pauseButton->addTouchEventListener(CC_CALLBACK_2(UI::pauseGame, this));
	this->addChild(pauseButton);

	pauseBG = Sprite::create("Images/Windows/pauseBG.png");
	pauseBG->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.8));
	pauseBG->setOpacity(0);
	this->addChild(pauseBG);
	
	gameOverBG = Sprite::create("Images/Windows/gameoverBG.png");
	gameOverBG->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.8));
	gameOverBG->setOpacity(0);
	this->addChild(gameOverBG);

	gameOverBG = Sprite::create("Images/Windows/gameoverBG.png");
	gameOverBG->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.8));
	gameOverBG->setOpacity(0);
	this->addChild(gameOverBG);

	winBG = Sprite::create("Images/Windows/winBG.png");
	winBG->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.8));
	winBG->setOpacity(0);
	this->addChild(winBG);

	for (auto& child : this->getChildren())
	{
		child->setGlobalZOrder(5);
	}
	gameScene->addChild(this);
}

UI::~UI()
{
}

void UI::showGameOver(int score)
{
	auto scoreView = Label::createWithTTF("0", "fonts/Marker Felt.ttf", 28);
	scoreView->setTextColor(Color4B::WHITE);
	scoreView->enableOutline(Color4B::BLACK, 2);
	scoreView->setPosition(gameOverBG->getPositionX() - 25, gameOverBG->getPositionY() - 160);
	scoreView->setString("YOUR SCORE: " + std::to_string(score));
	scoreView->setGlobalZOrder(10);

	switch (gameScene->gameState)
	{
	case Game::GAMESTATE::GAMEOVER:
		gameOverBG->runAction(FadeIn::create(0.2));
		gameOverBG->addChild(scoreView);
		break;
	case Game::GAMESTATE::WIN:
		winBG->runAction(FadeIn::create(0.2));
		winBG->addChild(scoreView);
		break;
	default:
		break;
	}
		
	auto retry = MenuItemImage::create("Images/UI/replay.png", "Images/UI/replay.png", CC_CALLBACK_1(UI::retry, this));
	retry->setPosition(Vec2(-50, -55));
	auto mainMenu = MenuItemImage::create("Images/UI/home.png", "Images/UI/home.png", CC_CALLBACK_1(UI::goToMainMenu, this));
	mainMenu->setPosition(Vec2(50, -55));

	gameOverMenu = Menu::create(retry, mainMenu, NULL);
	gameOverMenu->setPosition(Vec2(gameOverBG->getPositionX(), gameOverBG->getPositionY()));
	for (auto& aButtons : gameOverMenu->getChildren())
		for (auto& aSprites : aButtons->getChildren())
			aSprites->setGlobalZOrder(10);

	gameOverMenu->runAction(FadeIn::create(0.2));
	this->addChild(gameOverMenu);
}

void UI::createPauseMenu()
{
	auto resume = MenuItemImage::create("Images/UI/resume.png", "Images/UI/resume.png", CC_CALLBACK_1(UI::resume, this));
	resume->setPosition(Vec2(-50, 30));
	auto retry = MenuItemImage::create("Images/UI/replay.png", "Images/UI/replay.png", CC_CALLBACK_1(UI::retry, this));
	retry->setPosition(Vec2(50, 30));
	auto settings = MenuItemImage::create("Images/UI/settings.png", "Images/UI/settings.png", CC_CALLBACK_1(UI::openSettings, this));
	settings->setPosition(Vec2(-50, -60));
	auto mainMenu = MenuItemImage::create("Images/UI/home.png", "Images/UI/home.png", CC_CALLBACK_1(UI::goToMainMenu, this));
	mainMenu->setPosition(Vec2(50, -60));

	pauseMenu = Menu::create(resume, retry, settings, mainMenu, NULL);
	pauseMenu->setPosition(Vec2(pauseBG->getPositionX(), pauseBG->getPositionY()));

	for (auto& aButtons : pauseMenu->getChildren())
		for (auto& aSprites : aButtons->getChildren())
			aSprites->setGlobalZOrder(10);

	pauseMenu->runAction(FadeIn::create(0.2));
	this->addChild(pauseMenu);
}

void UI::pauseGame(Ref* sender, ui::Widget::TouchEventType type)
{
	if (gameScene->gameState == Game::GAMESTATE::PLAYING)
	{
		switch (type)
		{
		case ui::Widget::TouchEventType::ENDED:
		{
			pauseBG->runAction(FadeIn::create(0.2));
			AudioEngine::pauseAll();
			if (gameScene->isRunning()){
				gameScene->gameState = Game::GAMESTATE::PAUSED;

				gameScene->pause();
				for (auto& child : gameScene->getChildren()){
					if (child != this)
						child->pause();
				}
				createPauseMenu();
			}
			break;
		}
		default:
			break;
		}
	}
}

void UI::resume(Ref* sender)
{
	gameScene->gameState = Game::GAMESTATE::PLAYING;
	AudioEngine::resumeAll();
	gameScene->resume();
	for (auto& child : gameScene->getChildren())
		child->resume();

	pauseBG->setOpacity(0);
	this->removeChild(pauseMenu);
}

void UI::retry(Ref* sender)
{
	gameScene->resume();
	for (auto& child : gameScene->getChildren())
		child->resume();

	switch (gameScene->gameState)
	{
	case Game::GAMESTATE::PAUSED:
		pauseBG->setOpacity(0);
		this->removeChild(pauseMenu);
		break;
	case Game::GAMESTATE::GAMEOVER:
		gameOverBG->setOpacity(0);
		gameOverBG->removeAllChildren();
		this->removeChild(gameOverMenu);
		break;
	case Game::GAMESTATE::WIN:
		winBG->setOpacity(0);
		winBG->removeAllChildren();
		this->removeChild(gameOverMenu);
		break;
	default:
		break;
	}

	gameScene->restart();
}

void UI::goToMainMenu(Ref* sender)
{
	auto transition = TransitionFade::create(0.2, MainMenu::createScene());
	Director::getInstance()->replaceScene(transition);
}

void UI::openSettings(Ref* sender)
{
	pauseMenu->pause();
	auto settingsWindow = new SettingsWindow();
	settingsWindow->setOnExitCallback([=]() {
		pauseMenu->resume();
	});
	this->addChild(settingsWindow);
}