#include "ArenaView.h"
#include "Helper.h"
using namespace cocos2d;

ArenaView::ArenaView(Scene* aParent)
{
	Vec2 origin = PANEL_SETTINGS->origin;
	Size visibleSize = PANEL_SETTINGS->visibleSize;

	auto bg = Sprite::create("Images/Game/bg.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));

	int dy = PANEL_SETTINGS->root["GameScreen"]["ButtonsFrame"]["dy"].asInt();
	auto buttonsFrame = Sprite::create("Images/Game/buttonsFrame.png");
	buttonsFrame->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + dy));
	buttonsFrame->setGlobalZOrder(1);

	aParent->addChild(bg);
	aParent->addChild(buttonsFrame);
}

ArenaView::~ArenaView()
{
}
