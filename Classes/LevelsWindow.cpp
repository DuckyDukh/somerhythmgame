#include "LevelsWindow.h"
#include "Game.h"
#include "Helper.h"
#include "ui/CocosGUI.h"

using namespace cocos2d;

LevelsWindow::LevelsWindow(Json::Value& json)
{
	Vec2 origin = PANEL_SETTINGS->origin;
	Size visibleSize = PANEL_SETTINGS->visibleSize;

	auto bg = Sprite::create("Images/Choose/BG.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	this->addChild(bg);

	int step = 250;
	Vector<MenuItem*> genresArray;
	for (auto& song : json["songs"])
	{
		std::string pathToImage = "Images/Choose/song.png";

		std::string pathToSong = json["name"].asString() + "/" + song["artist"].asString() + " - " + song["name"].asString();
		auto one = MenuItemImage::create(pathToImage, pathToImage, CC_CALLBACK_1(LevelsWindow::startGame, this, pathToSong));
		one->setPosition(Vec2(0, step));

		std::string text = song["artist"].asString() + "\n" + song["name"].asString();
		if (HIGHSCORE->getHighScore(pathToSong).asInt() > 0) 
		{
			text += "\nHigh Score: " + HIGHSCORE->getHighScore(pathToSong).asString();
		}
		
		auto label = Label::createWithTTF(text, "fonts/Marker Felt.ttf", 28);
		label->setTextColor(Color4B::WHITE);
		label->setPosition(bg->getPositionX() + one->getPosition().x, bg->getPositionY() + one->getPosition().y);

		this->addChild(label);
		genresArray.pushBack(one);
		step -= 110;
	}

	auto back = MenuItemImage::create("Images/UI/back.png", "Images/UI/back.png", CC_CALLBACK_1(LevelsWindow::goBack, this));
	back->setPosition(Vec2(0, step));
	genresArray.pushBack(back);

	auto genresMenu = Menu::createWithArray(genresArray);;
	genresMenu->setPosition(Vec2(bg->getPositionX(), bg->getPositionY()));

	this->addChild(genresMenu);
}

LevelsWindow::~LevelsWindow()
{
}

void LevelsWindow::goBack(Ref* sender)
{
	this->removeFromParent();
}

void LevelsWindow::startGame(Ref* sender, std::string song)
{
	auto transition = TransitionFade::create(0.2, Game::createScene(song));
	Director::getInstance()->replaceScene(transition);
}
