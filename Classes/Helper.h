#pragma once
#include "Constants.h"
#include "PanelSettings.h"
#include "Settings.h"
#include "DownloadManager.h"
#include "HighScore.h"

#define CONSTANTS Helper::getInstance().getConstants()
#define PANEL_SETTINGS Helper::getInstance().getPanelSettings()
#define SETTINGS Helper::getInstance().getSettings()
#define DOWNLOADER Helper::getInstance().getDownloadManager()
#define HIGHSCORE Helper::getInstance().getHighScore()

namespace cocos2d
{
	enum DIFFICULTY {
		EASY,
		NORMAL,
		HARD
	};
}

class Helper
{
public:
	static Helper& getInstance();
	Helper(Helper const&) = delete;
	void operator=(Helper const&) = delete;
	
	Constants* getConstants() { return constants; }
	cocos2d::PanelSettings* getPanelSettings() { return panelSettings; }
	cocos2d::Settings* getSettings() { return settings; }
	DownloadManager* getDownloadManager() { return downloadManager; }
	cocos2d::HighScore* getHighScore() { return highScore; }
private:
	Helper();
	~Helper();

	Constants* constants;
	cocos2d::PanelSettings* panelSettings;
	cocos2d::Settings* settings;
	DownloadManager* downloadManager;
	cocos2d::HighScore* highScore;
};