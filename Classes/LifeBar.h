#pragma once
#include "cocos2d.h"

namespace cocos2d
{
	class LifeBar : public Node
	{
	public:
		LifeBar();
		~LifeBar();
		void startDraining();
		void damage();
		void heal();
		void setDifficulty(int);
		void reset();
		float getLife() { return life; }
	private:
		void update(float delta);

		ProgressTimer* hpBar;
		Scene* parent;
		float life;
		float drainRate;
		float healingPower;
		float damagePower;
	};
}