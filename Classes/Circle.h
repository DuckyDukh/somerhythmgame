#pragma once
#include "cocos2d.h"

namespace cocos2d
{
	class Circle
	{
	public:
		Circle(int aLine);
		~Circle();
		void createAction();
		Sprite* getSprite() { return sprite; }
		void setTimeOnScreen(float aTime) { timeOnScreen = aTime; };
		void setHitTime(float aTime) { hitTime = aTime; }

		void pop();
		bool getPopped() { return popped; }
		bool getMissed() { return missed; }
		float getHitTime() { return hitTime; }
	private:
		Vec2 origin;
		Size visibleSize;
		Sprite* sprite;

		float timeOnScreen;
		float hitTime;
		bool popped;
		bool missed;
	};
}


