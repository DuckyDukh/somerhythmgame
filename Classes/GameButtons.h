#pragma once
#include <cocos2d.h>
#include <json/json.h>

namespace cocos2d
{
	class Circle;
	class LifeBar;
	class ScoreManager;
	class GameButtons : public Node
	{
	public:
		GameButtons(ScoreManager*, LifeBar*);
		~GameButtons();

		void createGameButtons(std::list<std::shared_ptr<Circle>>& aList);
	private:
		const static int BUTTON_NUM = 3;
		std::list<std::shared_ptr<Circle>>* list;
		LifeBar* lifeBar;
		ScoreManager* scoreManager;

		Vec2 origin;
		Size visibleSize;

		int perfect;
		int good;
	};
}

