#pragma once
#include <cocos2d.h>
#include <json/json.h>

namespace cocos2d
{
	class MainMenu : public Scene
	{
	public:
		MainMenu();
		~MainMenu();

		static cocos2d::Scene* createScene();
		virtual bool init();
		CREATE_FUNC(MainMenu);
	private:
		void playPressed(Ref* sender);
		void openSettings(Ref* sender);
	};
}

