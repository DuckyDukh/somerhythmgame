#include "GameButtons.h"
#include "Helper.h"
#include "Circle.h"
#include "ScoreManager.h"
#include "ui/CocosGUI.h"
#include "LifeBar.h"
#include "ScoreManager.h"
#include "Game.h"
using namespace cocos2d;

GameButtons::GameButtons(ScoreManager* aScoreManager, LifeBar* aLifeBar)
{
    scoreManager = aScoreManager;
    lifeBar = aLifeBar;

    perfect = CONSTANTS->json["hits"]["perfect"].asInt();
    good = CONSTANTS->json["hits"]["good"].asInt();

    origin = PANEL_SETTINGS->origin;
    visibleSize = PANEL_SETTINGS->visibleSize;
}

GameButtons::~GameButtons()
{
}

void GameButtons::createGameButtons(std::list<std::shared_ptr<Circle>>& aList)
{
    list = &aList;

    for (int i = 0; i < BUTTON_NUM; i++)
    {
        int dx = PANEL_SETTINGS->root["GameScreen"]["GameButtons"]["dx"][std::to_string(i)].asInt();
        int dy = PANEL_SETTINGS->root["GameScreen"]["GameButtons"]["dy"].asInt();

        auto button = ui::Button::create("Images/Game/button.png");
        button->setPosition(Vec2(origin.x + visibleSize.width / 2 + dx, origin.y + dy));

        button->addTouchEventListener([=](Ref* sender, ui::Widget::TouchEventType type) {
            switch (type)
            {
            case ui::Widget::TouchEventType::BEGAN:
            {
                break;
            }
            case ui::Widget::TouchEventType::ENDED:
            {
                for (auto& circle : *list)
                { 
                    if (button->getPosition().x == circle->getSprite()->getPosition().x)
                    {
                        int dist = abs(button->getPosition().y - circle->getSprite()->getPosition().y);
                        if (dist <= perfect)
                        {
                            circle->pop();
                            scoreManager->add(hit::PERFECT, button->getPosition().x);
                            lifeBar->heal();
                            return;
                        }
                        else if (dist <= good)
                        {
                            circle->pop();
                            scoreManager->add(hit::GOOD, button->getPosition().x);
                            return;
                        }
                    }
                    scoreManager->add(hit::MISS, button->getPosition().x);
                    lifeBar->damage();
                    return;
                }
                break;
            }
            default:
                break;
            }
        });
        this->addChild(button);
    }
}
