#pragma once
#include <cocos2d.h>
#include <json/json.h>

namespace cocos2d
{
	class ArenaView;
	class GameButtons;
	class CircleFactory;
	class UI;
	class LifeBar;
	class ScoreManager;
	class Game : public Scene
	{
	public:
		enum GAMESTATE
		{
			NONE,
			PLAYING,
			PAUSED,
			WIN,
			GAMEOVER
		};
		Game();
		~Game();

		static Scene* createScene(std::string aSong);
		virtual bool init();
		CREATE_FUNC(Game);
		void start();
		void restart();
		void cleanUp();

		int gameState;
	private:
		void update(float delta);

		CircleFactory* factory;
		ArenaView* arena;
		GameButtons* gameButtons;
		UI* ui;
		LifeBar* lifeBar;
		ScoreManager* scoreManager;

		int audioId;
		std::string path;
	};
}

