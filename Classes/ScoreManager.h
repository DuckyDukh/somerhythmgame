#pragma once
#include "cocos2d.h"

namespace cocos2d 
{
	enum hit {
		MISS,
		GOOD,
		PERFECT
	};

	class ScoreManager : public Node
	{
	public:
		ScoreManager();
		~ScoreManager();
		void add(int aState, int aPos);
		void resetCombo();
		void reset();
		int getScore() { return score; }
		int getCombo() { return combo; }
	private:
		void createComboView();
		void createScoreView();
		void createHitText(int aState, int aPos);
		void updateScore();

		Label* scoreView;
		Sprite* comboSprite;
		Label* comboView;
		Vec2 origin;
		Size visibleSize;
		int score;
		int combo;
	};
}
