#include "MainMenu.h"
#include "GenresWindow.h"
#include "Helper.h"
#include "ui/CocosGUI.h"
#include "SettingsWindow.h"
#include "DownloadManager.h"

using namespace cocos2d;

MainMenu::MainMenu()
{
}

MainMenu::~MainMenu()
{
}

Scene* MainMenu::createScene()
{
    return MainMenu::create();
}

bool MainMenu::init()
{
    Vec2 origin = PANEL_SETTINGS->origin;
    Size visibleSize = PANEL_SETTINGS->visibleSize;

    auto bg = Sprite::create("Images/MainMenu/BG.png");
    bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
    this->addChild(bg);

	auto play = MenuItemImage::create("Images/MainMenu/play.png", "Images/MainMenu/play.png", CC_CALLBACK_1(MainMenu::playPressed, this));
	play->setPosition(Vec2(0, 0));
	auto settings = MenuItemImage::create("Images/MainMenu/settings.png", "Images/MainMenu/settings.png", CC_CALLBACK_1(MainMenu::openSettings, this));
	settings->setPosition(Vec2(0, -80));

	auto mainMenu = Menu::create(play, settings, NULL);
	mainMenu->setPosition(Vec2(bg->getPositionX(), bg->getPositionY()));
	this->addChild(mainMenu);

	if (!DOWNLOADER->isValidVersion() || FileUtils::getInstance()->getDataFromFile("Levels/genres.json").getBytes() == 0)
		DOWNLOADER->getGenresJson();
    return true;
}

void MainMenu::openSettings(Ref* sender)
{
	this->pause();
	for (auto& child : this->getChildren())
		child->pause();
	auto settingsWindow = new SettingsWindow();
	settingsWindow->setOnExitCallback([=]() {
		this->resume();
		for (auto& child : this->getChildren())
			child->resume();
	});
	this->addChild(settingsWindow);
}

void MainMenu::playPressed(Ref* sender)
{
	this->pause();
	for (auto& child : this->getChildren())
		child->pause();
	auto genresView = new GenresWindow();
	genresView->setOnExitCallback([=]() {
		this->resume();
		for (auto& child : this->getChildren())
			child->resume();
	});
	this->addChild(genresView);
}