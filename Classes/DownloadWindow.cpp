#include "DownloadWindow.h"
#include "DownloadManager.h"
#include "Helper.h"

using namespace cocos2d;

DownloadWindow::DownloadWindow()
{
	artificialProgress = 0;

	Vec2 origin = PANEL_SETTINGS->origin;
	Size visibleSize = PANEL_SETTINGS->visibleSize;

	auto bg = Sprite::create("Images/Windows/download.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.8));
	this->addChild(bg);

	auto barView = Sprite::create("Images/UI/sliderFront.png");
	loadingBar = ProgressTimer::create(barView);

	loadingBar->setType(ProgressTimer::Type::BAR);
	loadingBar->setBarChangeRate(Vec2(1, 0));
	loadingBar->setPosition(Vec2(bg->getPositionX(), bg->getPositionY() - 35));
	loadingBar->setVisible(true);
	loadingBar->setPercentage(0);
	this->addChild(loadingBar);

	for (auto& child : this->getChildren())
		child->setGlobalZOrder(2);

	this->scheduleUpdate();
}

DownloadWindow::~DownloadWindow()
{
}

void DownloadWindow::update(float delta)
{
	artificialProgress += delta * 60;

	loadingBar->setPercentage(DOWNLOADER->getProgress() * artificialProgress);
	if (loadingBar->getPercentage() >= 100)
		this->removeFromParent();
}