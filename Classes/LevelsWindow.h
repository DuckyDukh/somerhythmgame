#pragma once
#include <cocos2d.h>
#include <json/json.h>

namespace cocos2d
{
	class LevelsWindow : public Layer
	{
	public:
		LevelsWindow(Json::Value& json);
		~LevelsWindow();
	private:
		void startGame(Ref* sender, std::string song);
		void goBack(Ref* sender);
	};
}
