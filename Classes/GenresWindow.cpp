#include "GenresWindow.h"
#include "Helper.h"
#include "ui/CocosGUI.h"
#include "DownloadManager.h"
#include "DownloadWindow.h"
#include "LevelsWindow.h"

using namespace cocos2d;

GenresWindow::GenresWindow()
{
	std::string path = "Levels/genres.json";
	std::string file = FileUtils::getInstance()->getStringFromFile(path);
	Json::Reader reader;
	if (!reader.parse(file, genresJson))
	{
		log("No genres json");
		return;
	}
	Vec2 origin = PANEL_SETTINGS->origin;
	Size visibleSize = PANEL_SETTINGS->visibleSize;

	auto bg = Sprite::create("Images/Choose/BG.png");
	bg->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2));
	this->addChild(bg);

	int step = 250;
	Vector<MenuItem*> genresArray;
	for (auto& genre : genresJson["genres"])
	{
		std::string pathToImage = "Images/Choose/genre.png";
		if (!FileUtils::getInstance()->isDirectoryExist("Levels/" + genre["name"].asString()))
			pathToImage = "Images/Choose/noGenre.png";

		auto one = MenuItemImage::create(pathToImage, pathToImage, CC_CALLBACK_1(GenresWindow::chooseGenre, this, genre));
		one->setPosition(Vec2(0, step));

		auto text = Label::createWithTTF(genre["name"].asString(), "fonts/Marker Felt.ttf", 36);
		text->setTextColor(Color4B::WHITE);
		text->setPosition(bg->getPositionX() + one->getPosition().x, bg->getPositionY() + one->getPosition().y);
		text->setLocalZOrder(1);

		this->addChild(text);
		genresArray.pushBack(one);
		step -= 60;
	}

	auto back = MenuItemImage::create("Images/UI/back.png", "Images/UI/back.png", CC_CALLBACK_1(GenresWindow::goBack, this));
	back->setPosition(Vec2(0, step));
	genresArray.pushBack(back);

	auto genresMenu = Menu::createWithArray(genresArray);;
	genresMenu->setPosition(Vec2(bg->getPositionX(), bg->getPositionY()));
	this->addChild(genresMenu);
}

GenresWindow::~GenresWindow()
{
}

void GenresWindow::chooseGenre(Ref* sender, Json::Value genre)
{
	if (FileUtils::getInstance()->isDirectoryExist("Levels/" + genre["name"].asString()))
	{
		createLevelsWindow(genre);
	}
	else
	{
		createDownloadWindow(sender);
		DOWNLOADER->downloadGenre(genre);
	}	
}

void GenresWindow::goBack(Ref* sender)
{
	this->removeFromParent();
}

void GenresWindow::createDownloadWindow(Ref* sender)
{
	this->pause();
	for (auto& child : this->getChildren())
		child->pause();

	auto downloadView = new DownloadWindow();

	downloadView->setOnExitCallback([=]() {
		this->resume();
		for (auto& child : this->getChildren())
			child->resume();

		auto button = dynamic_cast<MenuItemImage*>(sender);
		button->setNormalImage(Sprite::create("Images/Choose/genre.png"));
		button->setSelectedImage(Sprite::create("Images/Choose/genre.png"));
	});
	this->addChild(downloadView);
}

void GenresWindow::createLevelsWindow(Json::Value genre)
{
	this->pause();
	for (auto& child : this->getChildren())
	{
		child->pause();
		child->setVisible(false);
	}

	auto levelsView = new LevelsWindow(genre);

	levelsView->setOnExitCallback([=]() {
		this->resume();
		for (auto& child : this->getChildren())
		{
			child->resume();
			child->setVisible(true);
		}
	});
	this->addChild(levelsView);
}