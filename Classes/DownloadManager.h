#pragma once
#include <iostream>
#include <fstream>
#include <cocos2d.h>
#include <json/json.h>

class DownloadManager
{
public:
	DownloadManager();
	~DownloadManager();
	bool isValidVersion();
	void getGenresJson();
	void downloadGenre(Json::Value genre);
	float getProgress();

private:
	void downloadFile(std::string pathToFile, std::string url);

	static size_t writeCallback(void* contents, size_t size, size_t nmemb, void* userp);
	static size_t writeData(void* contents, size_t size, size_t nmemb, void* userp);
	static int progress_func(void* ptr, double TotalToDownload, double NowDownloaded, double TotalToUpload, double NowUploaded);
};