#include "Helper.h"

Helper& Helper::getInstance()
{
	static Helper instance;
	return instance;
}

Helper::Helper()
{
	constants = new Constants();
	panelSettings = new cocos2d::PanelSettings();
	settings = new cocos2d::Settings();
	downloadManager = new DownloadManager();
	highScore = new cocos2d::HighScore();
}

Helper::~Helper()
{
	delete constants;
	delete panelSettings;
	delete settings;
	delete downloadManager;
	delete highScore;
}