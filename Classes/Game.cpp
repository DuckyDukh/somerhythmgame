#include "Game.h"
#include "CircleFactory.h"
#include "ArenaView.h"
#include "GameButtons.h"
#include "ScoreManager.h"
#include "Helper.h"
#include "ui/CocosGUI.h"
#include "AudioEngine.h"
#include "UI.h"
#include "LifeBar.h"
#include "ScoreManager.h"

using namespace cocos2d;

std::string song;

Game::Game()
{
}

Game::~Game()
{
    delete arena;
    delete gameButtons;
    delete factory;
    delete ui;
    delete lifeBar;
    delete scoreManager;

    AudioEngine::uncache(path + "/song.mp3");
}

Scene* Game::createScene(std::string aSong)
{
    song = aSong;
    return Game::create();
}

bool Game::init()
{
    gameState = GAMESTATE::NONE;

    path = "Levels/" + song;

    Json::Value json;
    std::string file = FileUtils::getInstance()->getStringFromFile(path + "/info.json");
    Json::Reader reader;
    if (!reader.parse(file, json))
    {
        log("No such level exist");
        return false;
    }

    int difficulty = json["diffuculty"].asInt();

    factory = new CircleFactory(json["tabs"]);
    factory->setDifficulty(difficulty);

    arena = new ArenaView(this);

    scoreManager = new ScoreManager();
    this->addChild(scoreManager);

    lifeBar = new LifeBar();
    lifeBar->setDifficulty(difficulty);
    this->addChild(lifeBar);

    gameButtons = new GameButtons(scoreManager, lifeBar);
    gameButtons->createGameButtons(factory->circleList);
    this->addChild(gameButtons);

    ui = new UI(this);

    std::function<void(bool)> func = [=](bool isSuccess) 
    { 
        if (isSuccess) 
            start(); 
    };
    AudioEngine::preload(path + "/song.mp3", func);

    return true;
}

void Game::start()
{
    gameState = GAMESTATE::PLAYING;
    factory->startProducing();
    for (auto& circle : factory->circleList)
        this->addChild(circle->getSprite());

    this->scheduleUpdate();

    audioId = AudioEngine::play2d(path + "/song.mp3", false, SETTINGS->getVolume());
    AudioEngine::setFinishCallback(audioId, [=](int finishID, const std::string& file)
    {
        gameState = GAMESTATE::WIN;
        ui->showGameOver(scoreManager->getScore());
        HIGHSCORE->setScore(song, scoreManager->getScore());
        cleanUp();
    });

    lifeBar->startDraining();
}

void Game::update(float delta)
{
    factory->circleList.remove_if([this](std::shared_ptr<Circle> circle) 
    {
        if (circle->getMissed())
        {
            scoreManager->resetCombo();
            lifeBar->damage();
            return true;
        }
        return circle->getPopped();
    });
    if (lifeBar->getLife() <= 0) 
    {
        gameState = GAMESTATE::GAMEOVER;
        ui->showGameOver(scoreManager->getScore());
        HIGHSCORE->setScore(song, scoreManager->getScore());
        cleanUp();
    }
    AudioEngine::setVolume(audioId, SETTINGS->getVolume());
}

void Game::cleanUp()
{
    AudioEngine::stopAll();
    lifeBar->reset();
    scoreManager->reset();
    factory->circleList.clear();
    this->unscheduleAllCallbacks();
}

void Game::restart()
{
    cleanUp();

    this->runAction(Sequence::create(DelayTime::create(1.0), CallFunc::create([=]() {
        start();
    }), nullptr));
}