#include "PanelSettings.h"
using namespace cocos2d;

PanelSettings::PanelSettings()
{
    origin = Director::getInstance()->getVisibleOrigin();
    visibleSize = Director::getInstance()->getVisibleSize();

    Json::Reader reader;
    std::string file = FileUtils::getInstance()->getStringFromFile("Properties/panelSettings.json");
    if (!reader.parse(file, root))
    {
        return;
    }
}

PanelSettings::~PanelSettings()
{
}
