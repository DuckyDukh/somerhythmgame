#include "Settings.h"
#include "AudioEngine.h"
using namespace cocos2d;

Settings::Settings()
{
    auto& string = UserDefault::getInstance()->getStringForKey("settings");

    if (string.length() > 0)
    {
        Json::Reader reader;

        if (!reader.parse(string, root))
        {
            return;
        }
    }
}

Settings::~Settings()
{
}

void Settings::save()
{
    Json::StyledWriter writer;
    UserDefault::getInstance()->setStringForKey("settings", writer.write(root));
    UserDefault::getInstance()->flush();
}

float Settings::getVolume()
{
    return root["volume"].asFloat();
}

void Settings::setVolume(float value)
{
    root["volume"] = value;
    save();
}