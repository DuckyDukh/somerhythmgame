#pragma once
#include <cocos2d.h>
#include <json/json.h>

namespace cocos2d
{
	class PanelSettings
	{
	public:
		PanelSettings();
		~PanelSettings();
		Vec2 origin;
		Size visibleSize;

		Json::Value root;
	private:
	};
}

