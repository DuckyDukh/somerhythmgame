#include "HighScore.h"
using namespace cocos2d;

HighScore::HighScore()
{
    auto& string = UserDefault::getInstance()->getStringForKey("highScores");

    if (string.length() > 0)
    {
        Json::Reader reader;

        if (!reader.parse(string, root))
        {
            return;
        }
    }
}

HighScore::~HighScore()
{
}

void HighScore::save()
{
    Json::StyledWriter writer;
    UserDefault::getInstance()->setStringForKey("highScores", writer.write(root));
    UserDefault::getInstance()->flush();
}

void HighScore::setScore(std::string song, int value)
{
    if (root[song]["highScore"].asInt() < value)
    {
        root[song]["highScore"] = value;
    }
        
    save();
}

Json::Value HighScore::getHighScore(std::string song)
{
    return root[song]["highScore"];
}