#pragma once
#include <cocos2d.h>
#include "ui/CocosGUI.h"

namespace cocos2d
{
	class DownloadWindow : public Layer
	{
	public:
		DownloadWindow();
		~DownloadWindow();

		void update(float delta);
	private:
		ProgressTimer* loadingBar;

		float artificialProgress;
	};
}
