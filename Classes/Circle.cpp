#include "Circle.h"
#include "Helper.h"
using namespace cocos2d;

Circle::Circle(int aLine)
{
	timeOnScreen = 0;
	hitTime = 0;
	popped = false;
	missed = false;

	sprite = Sprite::create("Images/Game/circle.png");
	int dx = PANEL_SETTINGS->root["GameScreen"]["Circle"]["dx"][std::to_string(aLine)].asInt();
	int dy = PANEL_SETTINGS->root["GameScreen"]["Circle"]["dy"].asInt();
	origin = PANEL_SETTINGS->origin;
	visibleSize = PANEL_SETTINGS->visibleSize;

	sprite->setScale(0.7f);
	sprite->setGlobalZOrder(2);
	sprite->setPosition(Vec2(origin.x + visibleSize.width / 2 + dx, origin.y + visibleSize.height + dy));
}


Circle::~Circle()
{
	sprite->runAction(RemoveSelf::create());
}

void Circle::createAction()
{
	int y = PANEL_SETTINGS->root["GameScreen"]["Buttons"]["dy"].asInt();
	float dy = y / visibleSize.height;
	auto actionDelay = DelayTime::create(hitTime - timeOnScreen);

	auto actionMove = MoveTo::create(timeOnScreen, Vec2(sprite->getPositionX(), y));
	auto toScale = ScaleTo::create(timeOnScreen, 1);
	auto actionScale = EaseInOut::create(toScale, 1.3f);

	auto move = Spawn::createWithTwoActions(actionScale, actionMove);
	auto actionOut = MoveTo::create(timeOnScreen * dy, Vec2(sprite->getPositionX(), -50));

	auto callback = CallFunc::create([this]() {
		missed = true;
	});

	sprite->runAction(Sequence::create(actionDelay, move, actionOut, callback, nullptr));
}

void Circle::pop()
{
	popped = true;
}