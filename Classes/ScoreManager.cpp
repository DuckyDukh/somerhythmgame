#include "ScoreManager.h"
#include "Helper.h"
using namespace cocos2d;

ScoreManager::ScoreManager() {
	score = 0;
	combo = 0;

	origin = PANEL_SETTINGS->origin;
	visibleSize = PANEL_SETTINGS->visibleSize;

	createComboView();
	createScoreView();
}

ScoreManager::~ScoreManager()
{
}

void ScoreManager::add(int aState, int aPos)
{
	switch (aState)
	{
	case hit::PERFECT:
	{
		combo += 1;
		score += 100 * combo;
		break;
	}
	case hit::GOOD:
	{
		combo += 1;
		score += 50 * combo;
		break;
	}
	case hit::MISS:
	{
		combo = 0;
		break;
	}
	}
	createHitText(aState, aPos);
	updateScore();
}

void ScoreManager::reset()
{
	score = 0;
	combo = 0;
	updateScore();
}

void ScoreManager::createScoreView()
{
	int dx = PANEL_SETTINGS->root["GameScreen"]["Score"]["dx"].asInt();
	int dy = PANEL_SETTINGS->root["GameScreen"]["Score"]["dy"].asInt();

	scoreView = Label::createWithTTF("0", "fonts/Marker Felt.ttf", 36);
	scoreView->setTextColor(Color4B::BLACK);
	scoreView->enableOutline(Color4B::WHITE, 1);
	scoreView->setAnchorPoint(Vec2(0, 0.5));
	scoreView->setPosition(origin.x + dx, visibleSize.height + dy);

	this->addChild(scoreView);
}

void ScoreManager::createHitText(int aState, int aPos)
{
	int dy = PANEL_SETTINGS->root["GameScreen"]["Hit"]["dy"].asInt();
	auto actionMove = MoveBy::create(0.3, Vec2(0, 10));
	auto toScale = ScaleTo::create(0.3, 1);
	auto actionScale = EaseInOut::create(toScale, 0.5f);
	auto move = Spawn::createWithTwoActions(actionScale, actionMove);
	auto remove = RemoveSelf::create();

	std::string path = "Images/Game/" + std::to_string(aState) + ".png";
	auto image = Sprite::create(path);
	image->setPosition(origin.x + aPos, origin.y + dy);
	image->setScale(0.6);

	image->runAction(Sequence::create(move, remove, nullptr));

	this->addChild(image);
}

void ScoreManager::createComboView()
{
	int dx = PANEL_SETTINGS->root["GameScreen"]["Combo"]["dx"].asInt();
	int dy = PANEL_SETTINGS->root["GameScreen"]["Combo"]["dy"].asInt();

	comboSprite = Sprite::create("Images/Game/combo.png");
	comboSprite->setPosition(visibleSize.width + dx, visibleSize.height + dy);

	comboView = Label::createWithTTF("0", "fonts/Marker Felt.ttf", 48);
	comboView->setTextColor(Color4B::ORANGE);
	comboView->enableOutline(Color4B::WHITE, 4);
	comboView->setAnchorPoint(Vec2(0.5, 0.0));
	comboView->setPosition(visibleSize.width + dx, visibleSize.height + dy + 10);

	comboSprite->setVisible(false);
	comboView->setVisible(false);

	this->addChild(comboSprite);
	this->addChild(comboView);
}

void ScoreManager::updateScore()
{
	scoreView->setString(std::to_string(score));

	bool isVisible;
	if (combo >= 1)
	{
		comboView->setString(std::to_string(combo));
		isVisible = true;
	}
	else
		isVisible = false;

	comboSprite->setVisible(isVisible);
	comboView->setVisible(isVisible);
}

void ScoreManager::resetCombo()
{
	combo = 0;
	updateScore();
}