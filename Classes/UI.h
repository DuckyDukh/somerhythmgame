#pragma once
#include <cocos2d.h>
#include "ui/CocosGUI.h"

namespace cocos2d
{
	class Game;
	class UI : public Node
	{
	public:
		UI(Game* aParent);
		~UI();

		void pauseGame(Ref* sender, ui::Widget::TouchEventType type);
		void resume(Ref* sender);
		void retry(Ref* sender);
		void goToMainMenu(Ref* sender);
		void openSettings(Ref* sender);
		void showGameOver(int score);
	private:
		void createPauseMenu();

		Game* gameScene;

		Sprite* pauseBG;
		Menu* pauseMenu;
		Sprite* gameOverBG;
		Sprite* winBG;
		Menu* gameOverMenu;

		Vec2 origin;
		Size visibleSize;
	};
}

