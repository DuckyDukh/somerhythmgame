#define CURL_STATICLIB
#include "DownloadManager.h"
#include "Helper.h"
#include <json/json.h>
#include <curl/curl.h>
#include <stdio.h>
#include <thread>

float progress = 0;

DownloadManager::DownloadManager()
{
}

DownloadManager::~DownloadManager()
{
}

size_t DownloadManager::writeCallback(void* contents, size_t size, size_t nmemb, void* userp)
{
    ((std::string*)userp)->append((char*)contents, size * nmemb);
    return size * nmemb;
}

size_t DownloadManager::writeData(void* contents, size_t size, size_t nmemb, void* userp)
{
    FILE* stream = (FILE*)userp;
    if (!stream)
    {
        return 0;
    }
    size_t written = fwrite(contents, size, nmemb, stream);
    return written;
}

int DownloadManager::progress_func(void* ptr, double TotalToDownload, double NowDownloaded, double TotalToUpload, double NowUploaded)
{
    if (TotalToDownload <= 0)
    {
        return 0;
    }

    progress = NowDownloaded / TotalToDownload;
    return 0;
}

void DownloadManager::downloadFile(std::string pathToFile, std::string url)
{
    FILE* fp;
    CURL* curl;
    CURLcode res;
    std::string path = cocos2d::FileUtils::getInstance()->getDefaultResourceRootPath();

    curl = curl_easy_init();
    if (curl)
    {
        fp = fopen((path + pathToFile).c_str(), "wb");

        curl_easy_setopt(curl, CURLOPT_URL, url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeData);
        curl_easy_setopt(curl, CURLOPT_NOPROGRESS, FALSE);
        curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, progress_func);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
        fclose(fp);
    }
}

bool DownloadManager::isValidVersion()
{
    CURL* curl;
    CURLcode res;
    std::string readBuffer;

    curl = curl_easy_init();
    if (curl)
    {
        curl_easy_setopt(curl, CURLOPT_URL, "http://localhost:8000/files/version.json");
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, writeCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);
        res = curl_easy_perform(curl);
        curl_easy_cleanup(curl);
    }

    Json::Value json;
    Json::Reader reader;
    if (reader.parse(readBuffer, json))
    {
        if (json["version"] == CONSTANTS->json["version"])
            return true;
    }
    CONSTANTS->json["version"] = json["version"];
	return false;
}

void DownloadManager::getGenresJson()
{
    downloadFile("levels/genres.json", "http://localhost:8000/files/genres.json");
}

void DownloadManager::downloadGenre(Json::Value genre)
{
    std::string root = cocos2d::FileUtils::getInstance()->getDefaultResourceRootPath();

    for (auto& song : genre["songs"])
    {
        std::string path = "Levels/" + genre["name"].asString() + "/" + song["artist"].asString() + " - " + song["name"].asString();
        cocos2d::FileUtils::getInstance()->createDirectory(root + path);

        std::thread thr([=]() 
        {
            downloadFile(path + "/song.mp3", song["song"].asString());
            downloadFile(path + "/info.json", song["info"].asString());
        });
        thr.detach();
    }
}

float DownloadManager::getProgress()
{
    return progress;
}