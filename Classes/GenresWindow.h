#pragma once
#include <cocos2d.h>
#include <json/json.h>

namespace cocos2d
{
	class GenresWindow : public Layer
	{
	public:
		GenresWindow();
		~GenresWindow();
	private:
		void createDownloadWindow(Ref* sender);
		void createLevelsWindow(Json::Value genre);

		void chooseGenre(Ref* sender, Json::Value genre);
		void goBack(Ref* sender);
		Json::Value genresJson;
	};
}
