#pragma once
#include "cocos2d.h"
#include <json/json.h>

namespace cocos2d
{
	class HighScore
	{
	public:
		HighScore();
		~HighScore();
		void setScore(std::string song, int value);
		Json::Value getHighScore(std::string song);
	private:
		void save();

		Json::Value root;
	};
}